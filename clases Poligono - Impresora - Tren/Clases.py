#Clase 4
#Ejercicios:
    #1) Clase poligono
    #2) Clase tren
    #3) Clase impresora

    
from os import system
#Figuras
system ("cls")
print ("********Clase POLIGONO********")
class Poligono:
    #Define un polígono según su base y su altura.

    def __init__(self, b, h):
        self.b = b
        self.h = h

class Rectangulo(Poligono):

    def area(self):
        return self.b * self.h
    
    def perimetro(self):
        return 2 * (self.b + self.h)

class Triangulo(Poligono):

    def area(self):
        return (self.b * self.h) / 2


base = int(input("Ingrese valor de la base: "))
altura = int(input("Ingrese valor de la altura: "))
rectangulo = Rectangulo(base, altura)
triangulo = Triangulo(base, altura)

print("Área del rectángulo: ", rectangulo.area())
print ("Perímetro del rectangulo: ", rectangulo.perimetro())
print("Área del triángulo:", triangulo.area())


#Clase Tren

class Vehiculo:
    """
    Define un vehiculo
    """
    print(".")
    print ("********Clase TREN********")
    def __init__(self):
        self.combustible = input("Ingrese el tipo de combustible: ")
        self.ruedas = int(input("Ingrese cantidad de ruedas: "))
        self.pasajeros = int(input("Ingrese cantidad de pasajeros: "))

class Tren(Vehiculo):

    def __init__(self):
        super().__init__()
        self.precio=float(input("Ingrese el precio:"))

    def ImpTotalViaje(self):
        return float(self.pasajeros * self.precio)

#Uso la instancia
tren1= Tren()
print("Tren N1. Tiene ",  tren1.ruedas, "ruedas, anda a ", tren1.combustible ," y el monto del viaje es: ", tren1.ImpTotalViaje())

tren2= Tren()
print("Tren N1. Tiene ",  tren2.ruedas, "ruedas, anda a ", tren2.combustible ," y el monto del viaje es: ", tren2.ImpTotalViaje())


#Impresora
print(".")
print ("********Clase IMPRESORA********")
class Aparato:
    def __init__ (self):
        self.voltaje = int(input("Ingrese los watts consumidos: "))

    def consumo (self):
        if self.voltaje > 1000:
            return "costoso"
        else:
            return "barato"


class Impresora(Aparato):
    def __init__(self):
        super().__init__()
        self.codtipo=int(input("Ingrese 1 si es impresora; 2 si es multifucion:"))

    def tipo (self):
        if self.codtipo == 1:
            return "Impresora"
        elif self.codtipo == 2:
            return "Multifuncion"
        else:
            return "No es una impresora"


impresora1 = Impresora()
print("Tipo: ", impresora1.tipo())
print("Usarla es ", impresora1.consumo())
