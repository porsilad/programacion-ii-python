from os import system
system ("cls")

#------------ESPECIFICACIONES DEL JUEGO------------
#JUGADOR: "$""
#PARED: "P"
#AGUJEROS: "_"
#CAJA: "#"
#EL OBJETIVO DEL JUEGO ES EMPUJAR LAS CAJAS HASTA UBICARLAS EN LOS AGUJEROS
#NO ES POSIBLE MOVER UNA CAJA A DONDE HAY UNA PARED
#LAS TECLAS DE MOVIMIENTO SON:
    #A: IZQUIERDA
    #S:ABAJO
    #D: DERECHA
    #W:ARRIBA
#--------------------------------------------------

#SETEOS
matriz = []
cantidad_filas = int(input("Ingrese la cantidad de filas del juego: "))
cantidad_columnas = int(input("Ingrese la cantidad de columnas del juego: "))

#posicion inicial caja
fila_caja_ant = 0
fila_caja = 3
col_caja_ant= 0
col_caja = 3
caja = "#"

#posicion agujero
fila_agu_ant = 0
fila_agu = 4
col_agu_ant= 0
col_agu = 4
agujero = "_"

#posicion inicial jugador
columna_ant=1
fila_ant=1
columna=1
fila=1
jugador = "$"

movimiento = "Z"


#Cargo la matriz vacia
for i in range(cantidad_filas):
    matriz.append([" "]*cantidad_columnas)

for i in range(cantidad_columnas):
    matriz[0][i]= "P"   #primer fila
    matriz[cantidad_filas-1][i]= "P"#ultima fila

for i in range(cantidad_filas):
    matriz[i][0]="P"#primer columna
    matriz[i][cantidad_columnas-1]="P"#ultima columna

matriz[5][5] = "P"
matriz[3][4] = "P"

#Posicion inicial del jugador $
matriz[fila][columna]=jugador

#Posicon inicial caja 
matriz[fila_caja][col_caja] = caja

#Posicion agujero
matriz[fila_agu][col_agu] = agujero


#Muestro la matriz
for i in range(cantidad_filas):
    print (matriz[i])


#Juego!
while not(fila_caja == fila_agu and col_caja == col_agu):#mientras no ponga la caja en el agujero

    movimiento = input("Muevase! (e para salir): ")
    if movimiento == "e":
        print ("Adios")
        break
    else:   
        system ("cls")
        columna_ant= columna
        fila_ant = fila

        if movimiento == "a":
            if matriz[fila][columna-1]!= "P":#No hay pared. columna + 1 = IZQUIERDA
                columna = columna -1
                if columna == col_caja and fila == fila_caja:
                    col_caja = col_caja - 1
            else:
                print ("HAY PARED")

        elif movimiento == "d":
            if matriz[fila][columna+1]!= "P":#No hay pared.  columna - 1 = DERECHA
                columna = columna +1
                if columna == col_caja and fila == fila_caja:
                    col_caja = col_caja + 1      
            else:
                print ("HAY PARED") 

        elif movimiento == "s":
            if matriz[fila+1][columna]!= "P":#No hay pared. fila + 1 = ABAJO 
                fila = fila +1
                if columna == col_caja and fila == fila_caja:
                    fila_caja = fila_caja +1
            else:
                print ("HAY PARED") 
        elif movimiento == "w":
            if matriz[fila-1][columna]!= "P":#No hay pared. fila - 1 = ARRIBA       
                fila = fila -1
                if columna == col_caja and fila == fila_caja:
                    fila_caja = fila_caja -1
            else:
                print ("HAY PARED")
                        
        else:
            print ("No te estas moviendo")



        matriz[fila_caja][col_caja]= caja

        matriz[fila_ant][columna_ant]=" "
        matriz[fila][columna]=jugador



        for i in range(cantidad_filas):
            print (matriz[i])
        
        if fila_caja == fila_agu and col_caja == col_agu:
            print ("****************YOU WIN!!!****************")
        

#PENDIENTES:
#1- AGREGAR Y MANEJAR MAS CAJAS Y AGUJEROS. HAY QUE CAMBIAR LA LOGICA PARA HACERLO MAS DINAMICO
#2- ASI COMO ESTA PUEDO MOVER  UNA CAJA A UNA PARED
#3- CUANDO HAYAN MAS CAJAS, CHEQUEAR CUANDO TODAS ESTEN EN LOS AGUJEROS PARA RECIEN DAR POR GANADO EL JUEGO